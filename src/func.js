
function getSum(a, b) {                                    
        function carry(value, index) {
            if (!value) {                                          
                return;
            }
            this[index] = (this[index] || 0) + value;              
            if (this[index] > 9) {                                 
                carry.bind(this)(this[index] / 10 | 0, index + 1); 
                this[index] %= 10;                                 
            }
        }
       var array1 = a.split('').map(Number).reverse(),            
           array2 = b.split('').map(Number).reverse();            
           array1.forEach(carry, array2);                             
           return array2.reverse().join('');  
};



const getQuantityPostsByAuthor = (listOfPosts,authorName) => {
    let postsCount = 0;
    let commentsCount = 0;
    for (let post of listOfPosts) {
        if (post.author === authorName) {
            postsCount += 1;
        }
        if (post.comments) {
            for (let comment of post.comments) {
                if (comment.author === authorName) {
                    commentsCount += 1;
                }
            }
        }
    }
    return `Post:${postsCount},comments:${commentsCount}`
};

function isEmpty(xs) { return xs.length === 0; }
function first(xs) { return xs[0]; }
function rest(xs) { return xs.slice(1); }

function tickets(xs) {
    function loop(a, b, c, xs) {
        if (a < 0 || b < 0 || c < 0)
            return "NO";
        else if (isEmpty(xs))
            return "YES";
        else
            switch (first(xs)) {
                case 25:
                    return loop(a + 1, b, c, rest(xs));
                case 50:
                    return loop(a - 1, b + 1, c, rest(xs));
                case 100:
                    return (b > 0) ? loop(a - 1, b - 1, c + 1, rest(xs)) : loop(a - 3, b, c + 1, rest(xs));
            }
    }
    return loop(0, 0, 0, xs);
}
module.exports = {getSum, getQuantityPostsByAuthor, tickets};